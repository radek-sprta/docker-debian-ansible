# rsprta/debian-ansible [![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/rsprta/debian-ansible)](https://hub.docker.com/r/rsprta/debian-ansible) [![Docker Pulls](https://img.shields.io/docker/pulls/rsprta/debian-ansible)](https://hub.docker.com/r/rsprta/debian-ansible) [![Pipeline status](https://gitlab.com/radek-sprta/docker-debian-ansible/badges/master/pipeline.svg)](https://gitlab.com/radek-sprta/docker-debian-ansible/commits/master)

## Quick reference
- **Maintained by**: [Radek Sprta](https://gitlab.com/radek-sprta)
- **Where to get help**: [Repository Issues](https://gitlab.com/radek-sprta/docker-debian-ansible/-/issues)

## Description
Debian container for Ansible playbook and role testing in Molecule.

## Usage
The simplest way to run the container is the following command:

```bash
docker run --detach rsprta/debian-ansible
```

Or using `docker-compose.yml`:

```yaml
version: '3'
services:
  debian-ansible:
    container_name: debian-ansible
    image: rsprta/debian-ansible
    restart: unless-stopped
```

## Contact
- [mail@radeksprta.eu](mailto:mail@radeksprta.eu)- [incoming+radek-sprta/docker-debian-ansible@gitlab.com](incoming+radek-sprta/docker-debian-ansible@gitlab.com)

## License
GNU General Public License v3

## Credits
This package was created with [Cookiecutter][cookiecutter] from [cookiecutter-docker-multiarch](https://gitlab.com/radek-sprta/cookiecutter-docker-multiarch). It is based on image by [Jeff Geerling](https://github.com/geerlingguy).

[cookiecutter]: https://github.com/audreyr/cookiecutter
