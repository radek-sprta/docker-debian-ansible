ARG VERSION=bullseye
FROM debian:$VERSION

LABEL maintainer="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.authors="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.description="Debian container for Ansible playbook and role testing in Molecule"
LABEL org.opencontainers.image.documentation="https://gitlab.com/radek-sprta/docker-debian-ansible/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="GNU General Public License v3"
LABEL org.opencontainers.image.source="https://gitlab.com/radek-sprta/docker-debian-ansible"
LABEL org.opencontainers.image.title="rsprta/debian-ansible"
LABEL org.opencontainers.image.url="https://gitlab.com/radek-sprta/docker-debian-ansible"

ARG DEBIAN_FRONTEND=noninteractive

ENV pip_packages "ansible cryptography"

# Install dependencies.
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
       build-essential \
       libffi-dev \
       libssl-dev \
       python3-apt \
       python3-dev \
       python3-pip \
       python3-setuptools \
       python3-wheel \
       sudo \
       systemd \
       systemd-sysv \
       wget \
    && rm -rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc \
    && rm -Rf /usr/share/man \
    && apt-get clean

# Upgrade pip to latest version.
RUN pip3 install --upgrade pip

# Install Ansible via pip.
RUN pip3 install $pip_packages

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts

# Make sure systemd doesn't start agettys on tty[1-6].
RUN rm -f /lib/systemd/system/multi-user.target.wants/getty.target

VOLUME ["/sys/fs/cgroup"]
CMD ["/lib/systemd/systemd"]
